<?php

use App\Models\OrderStatus;
use Illuminate\Database\Seeder;

class OrderstatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::create([
            'status' => 'Klaar'
        ]);
        OrderStatus::create([
            'status' => 'Niet klaar'
        ]);
    }
}
