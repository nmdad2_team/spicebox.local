<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use Auth;

class dashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
       if (Auth::guest()) {
           return redirect('./login');
       } else {
           if ($request->user()->id !== 1) {
               return redirect('./login');
           }
       }

        return $next($request);
    }
}
