<?php
$page_path = ['Products'] ;
$page_url = 'Update';
?>

@extends('admin')
@section('content')
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Update product</div>
        </div>
        <div class="panel-body detail">
            <form method="POST" action="/admin/product/{{$product[0]->id}}/do_update" class="form-horizontal"
                  enctype="multipart/form-data" role="form">
                {!! csrf_field() !!}
                <label class="col-md-1 control-label text-center" for="imgURL">Image</label>
                <div class="col-md-11 text-center">
                    <input id="imgURL" name="imgURL" type="text" class="form-control input-md" required=""
                           value="{{$product[0]->imgURL}}"/>
                </div>

                <label class="col-md-1 control-label text-center" for="name">Name</label>
                <div class="col-md-11">
                    <input id="name" name="name" type="text" placeholder="Product name" class="form-control input-md"
                           required="" value="{{$product[0]->name}}">
                </div>

                <label class="col-md-1 control-label text-center" for="categorie">Categorie</label>
                <div class="col-md-11">
                    <select class="form-control" name="categorie">
                        @foreach ($categories as $c)
                            <option id="{{$c->categorie}}" value="{{$c->id_categorie}}">{{$c->categorie}}</option>
                        @endforeach
                    </select>
                </div>

                <label class="col-md-1 control-label text-center" for="name">Price</label>
                <div class="col-md-11">
                    <input id="price" name="price" type="number" class="form-control input-md" required="">
                </div>

                <label class="col-md-1 control-label text-center" for="btw">Btw</label>
                <div class="col-md-11">
                    <select class="form-control" name="btw">
                        @foreach ($btws as $b)
                            <option id="{{$b->btw}}"  value="{{$b->id_btw}}">{{$b->btw}}</option>
                        @endforeach
                    </select>
                </div>


                <label class="col-md-1 control-label text-center" for="promotion">Promotion</label>
                <div class="col-md-11">
                    <select class="form-control" name="promotion">
                        @foreach ($promotions as $p)
                            <option id="{{$p->promotion_description}}" value="{{$p->id_promotion}}">{{$p->promotion_description}}</option>
                        @endforeach
                    </select>
                </div>

                <label class="col-md-1 control-label text-center" for="name">Description</label>
                <div class="col-md-11">
                    <textarea class="form-control" name="description">{{$product[0]->description}}</textarea>
                </div>

                <label class="col-md-1 control-label text-center" for="name">Specifications</label>
                <div class="col-md-11">
                    <textarea class="form-control" name="specifications">{{$product[0]->specifications}}</textarea>
                </div>

                <div class="col-md-12 text-center">
                    <button id="submit" name="submit" class="btn btn-success ">Update</button>
                </div>
            </form>
        </div>
    </div>
    <script>
        document.getElementById("price").value = "{{$product[0]->price}}";
        document.getElementById({{$product[0]->btw}}).selected = true;
        document.getElementById("{{$product[0]->categorie}}").selected = true;
        document.getElementById("{{$product[0]->promotion_description}}").selected = true;


    </script>
@endsection