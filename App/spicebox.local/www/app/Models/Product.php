<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    // Relationships
    // =============
    /**
     * Many-to-One.
     *
     * @link https://laravel.com/docs/5.2/eloquent-relationships#one-to-many
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    
    public function btw()
    {
        return $this->belongsTo(Btw::class);
    }

    public function prices()
    {
        return $this->belongsTo(Price::class);
    }
    
    public function promotions()
    {
        return $this->belongsTo(Promotion::class);
    }
    
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}