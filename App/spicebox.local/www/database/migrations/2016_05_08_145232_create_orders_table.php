<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('totalprice');
            $table->string('orderdate');
            
            $table->integer('id_orderstatus')->unsigned();
            $table->integer('id_klant')->unsigned();


            // Foreign Keys
            $table->foreign('id_orderstatus')->references('id_orderstatus')->on('orderstatus');
            $table->foreign('id_klant')->references('id')->on('klanten');

            // Meta Data
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
