<?php

namespace App\Http\Controllers;

use App\Models\Klant;
use App\Http\Requests;

class KlantenController extends Controller
{
    public function index(){

        $klanten = Klant::
            join('adress', 'klanten.id', '=', 'adress.id_klant')
            ->leftJoin('postcodes', 'adress.id_postcode', '=', 'postcodes.id')
            ->leftJoin('woonplaatsen', 'postcodes.id_woonplaats', '=', 'woonplaatsen.id_woonplaats')
            ->orderBy('klanten.id')
            ->get();

        return view('admin.klanten',['klanten' => $klanten]);

    }
    public function destroy($id){
        Klant::destroy($id);
        return redirect('admin/klanten');
    }
    public function sort($sort){

        $klanten = Klant::
            join('adress', 'klanten.id', '=', 'adress.id_klant')
            ->leftjoin('postcodes', 'adress.id_postcode', '=', 'postcodes.id_postcode')
            ->leftjoin('woonplaatsen', 'postcodes.id_woonplaats', '=', 'woonplaatsen.id_woonplaats')
            ->orderBy($sort, 'asc')
            ->get();
        return view('admin.klanten',['klanten' => $klanten]);
    }
}
