<?php
use App\Models\Promotion;
use Illuminate\Database\Seeder;

class PromotionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Promotion::create([
            'promotion' => '90',
            'promotion_description' => '10 procent korting'
        ]);
        Promotion::create([
            'promotion' => '50',
            'promotion_description' => 'halve prijs'
        ]);
        Promotion::create([
        'promotion' => '75',
        'promotion_description' => '25 procent korting'
        ]);
        Promotion::create([
            'promotion' => '100',
            'promotion_description' => '-'
        ]);
    }
}
