<?php namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Klant;
use App\Http\Controllers\Controller;
use Request;
use Auth;


class APIController extends Controller {
    public function getProducts(){
        $products = Product::
            join('btws', 'products.id_btw', '=', 'btws.id_btw')
            ->join('promotions', 'products.id_promotion', '=', 'promotions.id_promotion')
            ->join('categories', 'products.id_categorie', '=', 'categories.id_categorie')
            ->get();
        return $products;
    }
    public function getProduct($id){
        $product = Product::
            join('btws', 'products.id_btw', '=', 'btws.id_btw')
            ->join('promotions', 'products.id_promotion', '=', 'promotions.id_promotion')
            ->join('categories', 'products.id_categorie', '=', 'categories.id_categorie')
            ->where('id', $id)
            ->get();
        return $product;
    }

    public function getUserID(Request $request) {
        if (! Auth::guest()) {
            $user = Auth::user()->id;
            return json_encode($user);
        }
        return "Log in failed";


    }
    public function register(Request $request) {
        $test = [];
        $data = $request->all();
        $test['status'] = "ok";
        $test['data'] =  $data;
        $test['naam'] =  $data['naam'];

        $rules = [
            'naam' => 'required|max:255',
        ];

        $this->validate($request, $rules);
        $klant = new Klant();
        $klant->naam =  $data['naam'];
        $klant->email =  $data['email'];
        $klant->password =  $data['password'];

        //$category = Category::find($request->get('category')['id']);
        //$post->category()->associate($category);

        //$user = User::find(1); // @todo Do not hardcode user.
        //$post->user()->associate($user);

        if ($klant->save()) {
            $test['klant'] = $klant;
        }
        
    }

}