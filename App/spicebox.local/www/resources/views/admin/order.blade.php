@extends('admin')
<?php $page_title = "Detail van order";

$page_path = ['Orders'] ;
$page_url = 'Order';
        $totaalBestelling = 0;
?>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <td>Product</td>
                    <td>Prijs per eenheid</td>
                    <td>BTW</td>
                    <td>Promotion</td>
                    <td>Sub price</td>
                    <td>Aantal</td>
                    <td>Totaalprijs</td>
                    </thead>
                    <tbody>
                    @foreach ($bestelling as $b)
                            <tr>
                                <td>{{$b->name}}</td>
                                <td>€{{$b->price}}</td>
                                <td>{{$b->btw}}%</td>
                                <td>{{100 - $b->promotion}}%</td>
                                <td>€{{$b->price * (1 + $b->btw / 100) * ($b->promotion / 100)}}</td>
                                <td>{{$b->aantal}}</td>
                                <td>€{{$b->aantal * $b->price * (1+ $b->btw / 100) * ($b->promotion / 100)}}</td>
                        <?php $totaalBestelling += $b->aantal * $b->price * (1+ $b->btw / 100) * ($b->promotion / 100) ?>
                                </tr>
                    @endforeach
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td><b>€<?php echo $totaalBestelling?></b></td>
                    </tr>
                    </tbody>
                </table>


            </div>
        </div>
    </div>


@endsection