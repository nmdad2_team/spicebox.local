<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKlantenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('klanten', function (Blueprint $table) {
            $table->increments('id');
            $table->text('naam');
            $table->integer('aantal_bestellingen');
            
            
            $table->integer('id_user')->unsigned();


            // Foreign Keys
            $table->foreign('id_user')->references('id')->on('users');


            $table->timestamps();
            $table->softDeletes();

        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('klanten');
    }

}
