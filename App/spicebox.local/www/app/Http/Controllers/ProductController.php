<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Product;
use App\Models\Category;
use App\Models\Btw;
use App\Models\Promotion;
use App\Http\Controllers\Controller;
use Request;


class ProductController extends Controller {


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        $products = Product::
            join('btws', 'products.id_btw', '=', 'btws.id_btw')
            ->join('promotions', 'products.id_promotion', '=', 'promotions.id_promotion')
            ->join('categories', 'products.id_categorie', '=', 'categories.id_categorie')
            ->get();
        return view('admin.products',['products' => $products]);
    }

    public function destroy($id){
        Product::destroy($id);
        return redirect('admin/products');
    }

    public function newProduct(){
        $categories = Category::get();
        $btws = Btw::get();
        $promotions = Promotion::get();
        return view('admin.new', ['categories' => $categories, 'btws' => $btws , 'promotions' => $promotions]);
}
    public function add() {
        $product  = new Product();
        $product->name = Request::input('name');
        $product->description =Request::input('description');
        $product->specifications =Request::input('specifications');
        $product->price =Request::input('price');
        $product->imgURL =Request::input('imageurl');
        $product->id_categorie = Request::input('categorie');
        $product->id_promotion = Request::input('promotion');
        $product->id_btw = Request::input('btw');
        $product->prijshistoriek =  '{"Originele prijs":"'. Request::input('price') .'"}';

        $product->save();

        return redirect('/admin/products');

    }
    public function do_update($id) {
        date_default_timezone_set('Europe/Amsterdam');
        $today = date( 'j-m-y H:i:s');
        $prijshistoriek = Product::where('id', $id)->pluck('prijshistoriek');
        $arr_prijshistoriek = (array)json_decode($prijshistoriek[0]);

        $arr_prijshistoriek[$today] = Request::input('price');


        Product::where('id', $id)
            ->update(array(
                'name' => Request::input('name'),
                'price' => Request::input('price'),
                'imgURL' => Request::input('imgURL'),
                'id_categorie' => Request::input('categorie'),
                'id_btw' => Request::input('btw'),
                'id_promotion' => Request::input('promotion'),
                'description' => Request::input('description'),
                'specifications' => Request::input('specifications'),
                'prijshistoriek' => json_encode($arr_prijshistoriek)
            ));

        return redirect('/admin/products');
    }


    public function sort($sort) {
        $products = Product::join('btws', 'products.id_btw', '=', 'btws.id_btw')
            ->join('promotions', 'products.id_promotion', '=', 'promotions.id_promotion')
            ->join('categories', 'products.id_categorie', '=', 'categories.id_categorie')
            ->orderBy($sort, 'asc')
            ->get();

        return view('admin.products',['products' => $products]);

    }

    public function detail($id) {
        $product = Product::join('btws', 'products.id_btw', '=', 'btws.id_btw')
            ->join('promotions', 'products.id_promotion', '=', 'promotions.id_promotion')
            ->join('categories', 'products.id_categorie', '=', 'categories.id_categorie')
            ->where('id', ($id))
            ->get();

        return view('admin.detail',['product' => $product]);
    }

    public function update($id) {
        $product = Product::join('btws', 'products.id_btw', '=', 'btws.id_btw')
            ->join('promotions', 'products.id_promotion', '=', 'promotions.id_promotion')
            ->join('categories', 'products.id_categorie', '=', 'categories.id_categorie')
            ->where('id', ($id))
            ->get();
        $categories = Category::get();
        $btws = Btw::get();
        $promotions = Promotion::get();

        return view('admin.update',['categories' => $categories,'product' => $product, 'btws' => $btws, 'promotions' => $promotions]);

    }
    public function prijshistoriek($id) {
        $prijshistoriek = Product::where('id', $id)->pluck('prijshistoriek');
        $arr_prijshistoriek = json_decode($prijshistoriek[0]);
        return view('admin.prijshistoriek', ["prijshistoriek" => $arr_prijshistoriek]);
    }


}