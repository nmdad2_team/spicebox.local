<?php
$page_path = ['Products'] ;
$page_url = 'Prijshistoriek';
?>

@extends('admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <table class="table table-striped">
                <thead>
                <td>Date</td>
                <td>Price</td>
                </thead>
                <tbody>
                @foreach ($prijshistoriek as $key => $value)
                    <tr>
                        <td>{{$key}}</td>
                        <td>{{$value}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection