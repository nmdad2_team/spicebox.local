﻿//Create a module to handle user related things
var userModule = angular.module('userMod', ['toastMod']);

//Create a directive for the more specific subscriptions
userModule.directive('profilesubscriptions', function () {

    return {
        restrict: 'E',
        scope: {
            subscriptions: '=',
            onUnsubscribe: '&',
            onCancelUnsubscribe: '&',
            isMarkedToUnsubscribe: '&'
        },
        link: function(scope, elem, attrs) {

            scope.isMarked = function (index) {
                return scope.isMarkedToUnsubscribe({ index: index });
            }
            scope.markForDelete = function (index) {
                console.log("Unsubscribe " + index);
                scope.onUnsubscribe({ index: index });
            }
            scope.unmarkForDelete = function (index) {
                scope.onCancelUnsubscribe({ index: index });
            }
        },
        templateUrl: 'htmlTemplates/subscriptions/subscriptions-profile.html'
    };
});
//Create a directive for the more specific subscriptions
userModule.directive('profilesaccessories', function () {

    return {
        restrict: 'E',
        scope: {
            accessories: '=',
            onRatingSelected: '&'
        },
        templateUrl: 'htmlTemplates/accessories/accessories-profile.html'
    };
});
userModule.directive('addressform', function() {

    return {
        restrict: 'E',
        scope: {
            address: '=',
            submit: '&',
            edit: '&',
            cancel: '&',
            disabled: '='
        },
        link: function(scope) {

            scope.onEditClick = function() {
                console.log('Starting edit mode');
                scope.edit();
            }
        },
        templateUrl: 'htmlTemplates/addressform.html'
    };
});

//Create a controller to manage the user (guest or logged in user)
userModule.controller('UserController', ['$http', function($http) {

    this.user = undefined;

    //Simple function to check if a user is logged in
    this.userLoggedIn = function () {
        return this.user != undefined;
    }
    this.userHasAddress = function() {
        return (this.user != undefined &&
        (this.user.address.name != undefined || this.user.address.name !== "") &&
        (this.user.address.street != undefined || this.user.address.street !== "") &&
        (this.user.address.county != undefined || this.user.address.county !== "") &&
        (this.user.address.postalCode != undefined || this.user.address.postalCode !== "") &&
        (this.user.address.city != undefined || this.user.address.city !== "") &&
        (this.user.address.country != undefined || this.user.address.country !== ""));
    }

    //Sets the data for current user
    this.loginUser = function (user) {

        this.user = user;
    }
    //Removes the data for current user (back to guest status)
    this.logoutUser = function () {
        this.user = undefined;
    }

    this.inEditMode = false;
    var backupData = {};
    this.startEditUser = function () {

        this.inEditMode = true;
        backupData.name = this.user.address.name;
        backupData.street = this.user.address.street;
        backupData.county = this.user.address.county;
        backupData.postalCode = this.user.address.postalCode;
        backupData.city = this.user.address.city;
        backupData.country = this.user.address.country;
        backupData.note = this.user.address.note;
    }
    this.cancelEditUser = function() {

        this.user.address = backupData;
        backupData = {};
        this.inEditMode = false;
    }
    //Add db user update logic here
    this.updateUser = function() {

       var data = {
           naam : this.user.address.name,
           straatnaam : this.user.address.street,
           postcode : this.user.address.postalCode,
           woonplaats : this.user.address.city,
           huisnummer: this.user.address.number
       };
        $http({
            method: 'POST',
            data: data,
            url: './api/klant/update/1'
        }).then(function(response) {
            console.log(response.data);
        }, function(response) {
            console.log('Failed: ' + response.status + " " + response.message);
        });


        backupData = {};
        this.inEditMode = false;
    };

    //ADDRESSES
    //==========
    this.getAddress = function() {

        if (this.user === undefined)
            return undefined;

        if (this.user.address === undefined)
            this.user.address = {};

        return this.user.address;
    }
    //SETTING THE ADDRESS DOES NOT SAVE IT IN THE DATABASE
    this.setAddress = function (address) {

        this.user.address = address;
    }

    //SUBSCRIPTIONS
    //==============
    this.subscriptions = function() {

        //Can't give subscriptions from a nonexisting user
        if (this.user != undefined && this.user.subscriptions != undefined)
            return this.user.subscriptions;

        return undefined;
    }
    this.hasSubscriptions = function() {

        if (this.user === undefined || this.user.subscriptions === undefined)
            return false;

        return this.user.subscriptions.length > 0;
    }

    //If a subscription is loaded from db, use this one !! DOES NOT SAVE TO DATABASE !!
    this.addSubscription = function (subscription) {

        //Can't subscribe a nonexisting user
        if (this.user === undefined)
            return;

        if (this.user.subscriptions === undefined)
            this.user.subscriptions = [];

        this.user.subscriptions.push(subscription);
    }
    //If a new subscription is bought, use this one => still requires db magic
    this.addNewSubscription = function (subscription) {

        //Can't subscribe a nonexisting user
        if (this.user === undefined)
            return;

        if (this.user.subscriptions === undefined)
            this.user.subscriptions = [];

        this.addSubscription(subscription);
        //TODO: push to db
    }
    this.removeSubscription = function(index) {

        if (index >= 0 && index < this.user.subscriptions.length) {
            this.user.subscriptions.splice(index, 1);
        }

    }

    //A variable to keep track of all subscriptions that need to be added/changed in database
    var changedSubscriptions = [];
    this.markForDelete = function(index) {

        if (index >= 0 && index < this.user.subscriptions.length) {
            changedSubscriptions.push({ subscription: this.user.subscriptions[index], index: index });
        }
    }
    this.unmarkForDelete = function (index) {
        if (index >= 0 && index < this.user.subscriptions.length) {

            for (var i = 0; i < changedSubscriptions.length; ++i) {
                if (changedSubscriptions[i].index === index) {
                    changedSubscriptions.splice(i, 1);
                    break;
                }
            }
        }
    }
    this.isMarkedForDelete = function(index) {
        if (index >= 0 && index < this.user.subscriptions.length) {

            for (var i = 0; i < changedSubscriptions.length; ++i) {
                if (changedSubscriptions[i].index === index) {
                    return true;
                }
            }
        }

        return false;
    }

    //HISTORY PURCHASES
    //==================
    this.purchases = function() {

        //Can't give accessories from a nonexisting user
        if (this.user === undefined)
            return undefined;

        return this.user.accessories;
    }
    this.hasPurchases = function() {

        if (this.user === undefined || this.user.accessories === undefined)
            return false;

        return this.user.accessories.length > 0;
    }
    //If a purchased accessory is loaded from db, use this !! DOES NOT SAVE TO DATABASE !!
    this.addPurchase = function(accessory) {

        //Can't add accessory to a nonexisting user
        if (this.user === undefined)
            return;

        if (this.user.accessories === undefined)
            this.user.accessories = [];

        //Check if user already has no current subscription
        this.user.accessories.push(accessory);
    }
    //If a new accessory is purchased, use this => Still requires db magic
    this.addNewPurchase = function (accessory, amount) {

        //Can't add accessory to a nonexisting user
        if (this.user === undefined)
            return;

        //Check if user already has no current subscription
        var newAccessory = {
            "accessory": accessory,
            "amount": amount,
            "purchaseDate": new Date(),
            "rating": -1,
            "status": "unprocessed"
        }

        this.addPurchase(newAccessory);
        //TODO: Add push to db
    }

    //On Change of rating => still requires db magic
    var changedAccessories = [];
    this.ratingChanged = function (accessory, rating) {

        //No point in adding the same accessory multiple times to changelist, so if we find it, we don't do sh*t
        for (var i = 0; i < changedAccessories.length; ++i) {

            if (changedAccessories[i] === accessory)
                return;
        }

        //Otherwise just add it
        changedAccessories.push(accessory);
    }

    //UPDATING DB
    //============
    this.hasUnsavedChanges = function() {
        return (changedSubscriptions.length > 0 || changedAccessories.length > 0);
    }

    //Add db subscriptions update logic here
    this.updateChanges = function() {

        //TODO: Add DB logic here
        window.alert("Database updating." +
                     "\nSubscription changes: " + changedSubscriptions.length +
                     "\nAccessory changes: " + changedAccessories.length);

        //Don't bother db if no changes happened
        if (changedSubscriptions.length <= 0 && changedAccessories.length <= 0)
            return;

        for (var i = 0; i < changedSubscriptions.length; ++i) {
            this.removeSubscription(changedSubscriptions[i].index);
        }

        //TODO: Add db update magic here

        //User feedback and changes reset
        changedSubscriptions = [];
        changedAccessories = [];
    }
}]);
//Create a controller to deal with the login form
userModule.controller('LoginController', ['$http', function($http) {

    //data will hold all input we need
    //Remove name, email and password to start with empty fields
    //(added them here so I don't have to fill it in every time)
    this.data = {
        name: 'xXx_Spice_Mastah_xXx',
        email: 'spicedSpicesSpice@spicey.com',
        password : 'spicey'
    };

    //Here you connect to the database to check if valid user
    this.logIn = function (mainCtrl, userCtrl) {
        var email  = $('#input_4').val();
        var password  = $('#input_5').val();


        var data = { email : email, password:password};
        $http({
            method: 'POST',
            data: data,
            url: './signin'
        }).then(function(response){
            console.log(response.data);
            localStorage.setItem('token', response.data.token);
            localStorage.setItem('session', "true");

            $http({
                method: 'GET',
                url: './api/user'
            }).then(function(response){
                //console.log(response.data);
                localStorage.setItem('id_user', response.data);

        }, function(response) {
            console.log('Failed: ' + response.status + " " + response.message);
        });

        }, function(response) {
            console.log('Failed: ' + response.status + " " + response.message);
        });
        
        //TEMPORARY
        if (localStorage.getItem("session") == "true") {

            $http({
                method: 'GET',
                url: './api/klant/' + localStorage.getItem('id')
            }).then(function(response){
                console.log(response);
console.log("aaa");

            }, function(response) {
                console.log('Failed: ' + response.status + " " + response.message);
                console.log("bbb");
            });

/*
            var loggedInUser = {
                name: 'ju',
                email: "email",
                address: {}
            };

            userCtrl.loginUser(loggedInUser);
            userCtrl.setAddress({
                name: 'ju',
                street: user.straatnaam,
                number: user.huisnummer,
                postalCode: user.postcode,
                city: user.woonplaats
            });

            mainCtrl.setLastPageActive(); //Go back to last page
            this.data = {}; //Clear the input boxes
            */
        }
    }
}]);

//Create a controller to deal with the register form
userModule.controller('RegisterController', ['$http', function($http) {

    //data will hold all input we need
    this.data = {};
    this.passwordMatches = function() {

        if (this.data.password === undefined || this.data.repeatPassword === undefined)
            return false;

        return this.data.password === this.data.repeatPassword;
    }

    //Here you connect to the databse and add the new user
        this.register = function (mainCtrl, userCtrl) {
        var email  = $('#input_1').val();
        var password  = $('#input_2').val();


        var data = { email : email, password:password};
        $http({
            method: 'POST',
            data: data,
            url: './register'
        }).then(function(response){
            console.log(response.data);
        }, function(response) {
            console.log('Failed: ' + response.status + " " + response.message);
        });
    }
}]);

//Create a controller to deal with the address form
userModule.controller('AddressController', function () {

    this.address = {};

    //Update to address in user
    this.updateUserAddress = function(userCtrl) {

        //No point in continuing if no user
        if (userCtrl === undefined || userCtrl === null)
            return;

        //Set address in userCtrl
        userCtrl.setAddress(this.address);
    }
});

//General helper functions
function GetNextMonth(getStartOfMonth) {

    //Get current date
    var d = new Date();

    //Set the day to start or end (based on input)
    if (getStartOfMonth) {
        d.setDate(1);
        d.setMonth(d.getMonth() + 1);
    } else {
        d.setDate(0);
        d.setMonth(d.getMonth() + 2);
    }

    return d;
}
