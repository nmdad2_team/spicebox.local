<?php

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Product::create([
            'name' => 'Parsley',
            'description' => 'Garden your own parsley plant for delicous fresh herbs',
            'specifications' => '1 parsley plant, 1 pot',
            'price' => '3',
            'imgURL' => 'http://image.shutterstock.com/z/stock-photo-kitchen-herb-parsley-in-pot-on-white-background-90773639.jpg',
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 1,
            'prijshistoriek' => '{"Originele prijs":"3"}',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5)
        ]);
        Product::create([
            'name' => 'Basil',
            'description' => 'Garden your own basil plant for delicous fresh herbs',
            'specifications' => '1 basil plant, 1 pot',
            'price' => '3',
            'prijshistoriek' => '{"Originele prijs":"3"}',
            'imgURL' => 'https://wwwdothardyherbsdotcom.files.wordpress.com/2013/09/fotolia_51419459_subscription_monthly_m.jpg',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5),
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 1,

        ]);
        Product::create([
            'name' => 'Thyme',
            'description' => 'Garden your own thyme plant for delicous fresh herbs',
            'specifications' => '1 thyme plant, 1 pot',
            'price' => '3',
            'prijshistoriek' => '{"Originele prijs":"3"}',
            'imgURL' => 'http://cdn.notonthehighstreet.com/system/product_images/images/001/081/831/original_lemon-thyme-herb-in-a-vintage-style-planter.jpg',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5),
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 1
        ]);
        Product::create([
            'name' => 'Garlic presser',
            'description' => 'This usefull machine presses your garlic into small pieces',
            'specifications' => '- 1 garlice presser',
            'price' => '15',
            'prijshistoriek' => '{"Originele prijs":"15"}',
            'imgURL' => 'http://rk.wsimgs.com/wsimgs/rk/images/dp/wcm/201605/0004/img12o.jpg',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5),
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 3
        ]);
        Product::create([
            'name' => 'Herb scissors',
            'description' => 'This is used to cut your herbs into small pieces',
            'specifications' => '1 Herb scissors',
            'price' => '20',
            'prijshistoriek' => '{"Originele prijs":"20"}',
            'imgURL' => 'http://static.yuppiechef.com/spatula/wp-content/uploads/2013/01/herb-scissors1.jpg',
            'aantal_verkocht' => rand(0,15),
            'id_promotion' => rand(1, 4),
            'rating' => rand(0,5),
            'id_btw' => rand(1, 4),
            'id_categorie' => 3
        ]);
        Product::create([
            'name' => 'Herb Grinder',
            'description' => 'This is used to grind your herbs into small pieces',
            'specifications' => "1 Herb Grinder, 2 AA batteries",
            'price' => '79',
            'prijshistoriek' => '{"Originele prijs":"79"}',
            'imgURL' => 'http://ecx.images-amazon.com/images/I/51SvdsSMb9L.jpg',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5),
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 3
        ]);
        Product::create([
            'name' => 'Chrome Spinning Spice Rack',
            'description' => 'Our polished Chrome Spice Rack brings commercial-style flair to your counter top. ',
            'specifications' => '1 spice rack, 6-glass bottles, chrome lids, 84 pre-printed labels',
            'price' => '45',
            'prijshistoriek' => '{"Originele prijs":"45"}',
            'imgURL' => 'http://cdn-eu-ec.yottaa.net/55df7e1a2bb0ac7d800040c2/ecdf7130ef10013390340a3ba3fac80a.yottaa.net/v~13.34/catalogimages/190816/ChromeSpinningSpiceRack_x.jpg?width=560&height=560&align=center&yocs=t_&yoloc=eu',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5),
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 2
        ]);

        Product::create([
            'name' => 'Magnetic Spice Rack',
            'description' => 'These clever canisters are a unique way to store small items like spices, herbs and other seasonings. Made of food safe tin, each lid features a clear window for quick identification of the contents.',
            'specifications' => 'Each canister measures 2-1/2" diam. x 1-7/8" h',
            'price' => '15',
            'prijshistoriek' => '{"Originele prijs":"15"}',
            'imgURL' => 'http://cdn-eu-ec.yottaa.net/55df7e1a2bb0ac7d800040c2/ecdf7130ef10013390340a3ba3fac80a.yottaa.net/v~13.34/catalogimages/263833/21004.jpg?width=560&height=560&align=center&yocs=t_&yoloc=eu',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5),
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 2
        ]);

        Product::create([
            'name' => 'Chrome 18-Bottle Spice Rack',
            'description' => 'Store your spice collection on our attractive Chrome 18-Bottle Spice Rack. Three tiers offer storage for the 18 bottles included. Each features a plastic sifter.',
            'specifications' => '18 glass bottles with chrome-plated lids',
            'price' => '69',
            'prijshistoriek' => '{"Originele prijs":"69"}',
            'imgURL' => 'http://cdn-eu-ec.yottaa.net/55df7e1a2bb0ac7d800040c2/ecdf7130ef10013390340a3ba3fac80a.yottaa.net/v~13.34/catalogimages/190808/18BottleSpiceRackChrome_x.jpg?width=560&height=560&align=center&yocs=t_&yoloc=eu',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5),
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 2
        ]);

        Product::create([
            'name' => "Guide to Herbs & Spices",
            'description' => "Nobody knows herbs and spices like Tony Hill, owner of Seattle's famed World Merchants Spice, Herb & Teahouse.",
            'specifications' => 'paperback, 432 pages, language: English',
            'price' => '25',
            'prijshistoriek' => '{"Originele prijs":"25"}',
            'imgURL' => 'http://ecx.images-amazon.com/images/I/51uBzptkpdL._SX406_BO1,204,203,200_.jpg',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5),
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 4
        ]);
        Product::create([
            'name' => 'Essential Spices and Herbs',
            'description' => 'Essential Spices and Herbs introduces you to the 50 must-know herbs and spices that will take your cooking to the next level.',
            'specifications' => 'paperback, 292 pages, language: English',
            'price' => '15',
            'prijshistoriek' => '{"Originele prijs":"15"}',
            'imgURL' => 'http://ecx.images-amazon.com/images/I/61xRE2aQPCL._SY498_BO1,204,203,200_.jpg',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5),
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 4
        ]);

        Product::create([
            'name' => 'Exclusive Spicebox apron',
            'description' => 'Get your cooking swag on with this one of a kind Spicebox apron.',
            'specifications' => '1 cotton apron',
            'price' => '20',
            'prijshistoriek' => '{"Originele prijs":"20"}',
            'imgURL' => 'http://users.telenet.be/timvanmeirvenne/spicebox/appron.png',
            'aantal_verkocht' => rand(0,15),
            'rating' => rand(0,5),
            'id_promotion' => rand(1, 4),
            'id_btw' => rand(1, 4),
            'id_categorie' => 4
        ]);



        // Faker
        // -----
        //factory(Product::class, DatabaseSeeder::AMOUNT['DEFAULT'])->create();
    }
}
