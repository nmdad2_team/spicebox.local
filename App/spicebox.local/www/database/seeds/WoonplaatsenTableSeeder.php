<?php

use App\Models\Woonplaats;
use Illuminate\Database\Seeder;

class WoonplaatsenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Woonplaats::create([
            'Woonplaats' => 'Brussel',
        ]);
        Woonplaats::create([
            'Woonplaats' => 'Antwerpen',
        ]);
        Woonplaats::create([
            'Woonplaats' => 'Leuven',
        ]);
    }
}
