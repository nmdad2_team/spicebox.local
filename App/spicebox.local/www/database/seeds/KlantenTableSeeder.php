<?php
use App\Models\Klant;
use Illuminate\Database\Seeder;

class KlantenTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Faker
        // -----
        factory(Klant::class, DatabaseSeeder::AMOUNT['DEFAULT'])->create();
    }
}
