<?php

use App\Models\Btw;
use Illuminate\Database\Seeder;

class BtwTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Btw::create([
            'btw' => '0'
        ]);
        Btw::create([
            'btw' => '6'
        ]);
        Btw::create([
            'btw' => '12'
        ]);
        Btw::create([
            'btw' => '21'
        ]);
    }
}
