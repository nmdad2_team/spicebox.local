<?php
/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/




Route::group(['middleware' => 'cors'], function () {

    Route::post('/registerklant', 'APIKlantenController@registerKlant');
    Route::post('/signin',   'APIKlantenController@signin');


    Route::get('api/products', 'APIController@getproducts');
    Route::get('api/product/{id}', 'APIController@getproduct');



    Route::group(['middleware' => 'api'], function () {
        /* API */
        Route::get('api/user', 'APIController@getuserID');
        Route::get('api/klant', 'APIKlantenController@getKlant');
        Route::resource('order', 'ApiOrdersController');
        Route::resource('orderProduct', 'ApiOrderProductsController');
        Route::post('api/klant/update', 'APIKlantenController@updateKlant');


    });
});

Route::group(['middleware' => 'web'], function () {

    // Authentication routes...
    Route::get('auth/login', 'Auth\AuthController@getLogin');
    Route::post('auth/login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

    Route::auth();

        Route::group(['middleware' => 'dashboard'], function () {
            
            /* Dashboard Controller */
            Route::get('admin/dashboard', 'DashboardController@index');
            Route::get('/', 'DashboardController@index');

        /* Product Controller Router */
        Route::get('/admin/products', 'ProductController@index');
        Route::get('/admin/product/new', 'ProductController@newProduct');
        Route::get('/admin/product/destroy/{id}', 'ProductController@destroy');
        Route::get('/admin/products/sort/{sort}', 'ProductController@sort');
        Route::get('/admin/product/detail/{id}', 'ProductController@detail');
        Route::get('/admin/product/detail/{id}/update', 'ProductController@update');
        Route::get('/admin/product/prijshistoriek/{id}', 'ProductController@prijshistoriek');

        Route::post('/admin/product/save', 'ProductController@add');
        Route::post('/admin/product/{id}/do_update', 'ProductController@do_update');

        /* Klanten Controller Router */

        Route::get('/admin/klanten', 'KlantenController@index');
        Route::get('/admin/klant/destroy/{id}', 'KlantenController@destroy');
        Route::get('/admin/klanten/sort/{sort}', 'KlantenController@sort');

        /* Orders Controller Router */
        Route::get('/admin/orders', 'OrdersController@index');
        Route::get('/admin/order/{id}/finish','OrdersController@finish');
        Route::get('/admin/order/{id}','OrdersController@detail');
    });

});
