<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\OrderedProduct;
use App\Models\Order;
use Auth;


class ApiOrderProductsController extends Controller
{
    public function index(){
        return OrderedProduct::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $orderedProduct = new OrderedProduct();
        $orderedProduct->aantal = $data['amount'];
        $orderedProduct->id_product = $data['id'];
        $orderedProduct->id_order = $data['id_order'];


        $orderedProduct->save();
        return $orderedProduct;


    }

}
