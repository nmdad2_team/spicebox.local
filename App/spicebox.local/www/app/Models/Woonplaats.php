<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Woonplaats extends Model
{
    use SoftDeletes;
    protected $table = 'woonplaatsen';
    protected $fillable = array('woonplaats');

    // Relationships
    // =============
    /**
     * Many-to-One.
     *
     * @link https://laravel.com/docs/5.2/eloquent-relationships#one-to-many
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */


    public function user()
    {
        return $this->belongsTo(User::class);
    }
}