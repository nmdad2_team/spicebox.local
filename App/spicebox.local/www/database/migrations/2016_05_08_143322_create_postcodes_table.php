<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostcodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postcodes', function (Blueprint $table) {

            // Data
            $table->increments('id');
            $table->integer('postcode');
            $table->integer('id_woonplaats')->unsigned();


            // Foreign Keys
            $table->foreign('id_woonplaats')->references('id_woonplaats')->on('woonplaatsen');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('postcodes');
    }
}
