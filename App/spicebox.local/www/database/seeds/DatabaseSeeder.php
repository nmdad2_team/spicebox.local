<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    const AMOUNT = [
        'NONE' => 0,
        'MIN' => 1,
        'FEW' => 3,
        'SOME' => 5,
        'DEFAULT' => 10,
        'MANY' => 20,
        'MAX' => 1000,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Seeder order is important for database relations!
        $seeders = [
            UsersTableSeeder::class,
            CategoriesTableSeeder::class,
            BtwTableSeeder::class,
            PromotionsTableSeeder::class,
            ProductsTableSeeder::class,
            WoonplaatsenTableSeeder::class,
            PostcodesTableSeeder::class,
            KlantenTableSeeder::class,
            AdressTableSeeder::class,
            OrderstatusTableSeeder::class,
            OrdersTableSeeder::class,
            OrderedProductsSeeder::class,
        ];

        $i = 0;
        foreach ($seeders as $seeder) {
            $count = sprintf('%02d', ++$i);
            $this->command->getOutput()->writeln("<comment>Seed${count}:</comment> ${seeder}...");
            $this->call($seeder);
        }
    }
}
