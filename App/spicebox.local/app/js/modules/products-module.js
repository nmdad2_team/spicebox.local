﻿//Create a module to contain all data for the offered products
var productsModule = angular.module('productsMod', []);

//DIRECTIVES
//===========
productsModule.directive('subscription', function () {

    return {
        restrict: 'E',
        scope: {
            subscription: '=',
            mainCtrl: '='
        },
        link: function (scope, elem, attrs) {

            scope.redirect = function (subscription) {

                if(scope.mainCtrl != undefined)
                    scope.mainCtrl.setSubscriptionPageActive(subscription);
          }
        },
        templateUrl: 'htmlTemplates/subscriptions/subscription.html'
    };
});
productsModule.directive('subscriptionDetail', function () {

    return {
        restrict: 'E',
        scope: {
            subscription: '='
        },
        templateUrl: 'htmlTemplates/subscriptions/subscription-detail.html'
    };
});
productsModule.directive('subscriptions', function () {

    return {
        restrict: 'E',
        scope: {
            subscriptions: '=',
            cartCtrl: '=',
            mainCtrl: '=',
            toastCtrl: '='
        },
        templateUrl: 'htmlTemplates/subscriptions/subscriptions.html',
        link: function (scope, elem, attrs) {

            scope.AddToCart = function (index) {
                scope.cartCtrl.addSubscription(scope.subscriptions[index]);
                scope.toastCtrl.showAddToCart(scope.subscriptions[index]);
            };
        }
    };
});

productsModule.directive('accessory', function () {

    return {
        restrict: 'E',
        scope: {
            accessory: '=',
            mainCtrl: '='
        },
        link: function (scope, elem, attrs) {

            scope.redirect = function (accessory) {

                if (scope.mainCtrl != undefined)
                    scope.mainCtrl.setAccessoryPageActive(accessory);
            }

            scope.getBtwPrice = function(accessory) {
              return accessory.price + (accessory.price * (accessory.btw/100))
            }

            scope.getPromotedPrice = function(accessory) {
              var price = accessory.price * (accessory.promotion/100)

              return price + (price * (accessory.btw/100))
            }
        },
        templateUrl: 'htmlTemplates/accessories/accessory.html'
    };
});
productsModule.directive('accessoryDetail', function () {

    return {
        restrict: 'E',
        scope: {
            accessory: '='
        },
        templateUrl: 'htmlTemplates/accessories/accessory-detail.html'
    };
});
productsModule.directive('accessories', function () {

    return {
        restrict: 'E',
        scope: {
            accessories: '=',
            cartCtrl: '=',
            mainCtrl: '=',
            toastCtrl: '='
        },
        templateUrl: 'htmlTemplates/accessories/accessories.html',
        link: function (scope, elem, attrs) {

            scope.addToCart = function (index) {
                scope.cartCtrl.addAccessory(scope.accessories[index]);
                scope.toastCtrl.showAddToCart(scope.accessories[index]);
            };
        }
    };
});

productsModule.directive('starRating', function () {
    return {
        restrict: 'A',
        scope: {
            ratingValue: '=',
            canChangeRating: "=",
            max: '=',
            onRatingSelected: '&',
            product: '='
        },
        templateUrl: 'htmlTemplates/accessories/starRatings.html',
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function (index) {

                if (!scope.canChangeRating)
                    return;

                scope.ratingValue = index + 1;
                scope.onRatingSelected(scope.product, index + 1);
            };

            scope.$watch('ratingValue', function (oldVal, newVal) {

                if (oldVal === 0) {
                    updateStars();
                }

                if (newVal) {
                    updateStars();
                }
            });
        }
    }
});

//CONTROLLERS
//============
//Create a controller to control all products (subscriptions and accessoiries) in store
productsModule.controller('ProductsController', ['$scope', '$http', function ($scope, $http) {
    $http.defaults.headers.common.Authorization = 'Bearer ' + localStorage.getItem('token');

    
    $http({
        method: 'GET',
        url: 'http://api.spicebox.local/api/products'
    }).then(successAccessories, error);

    //In case the reading works, we call this function
    function successSubscriptions(response) {
        $scope.subscriptions = response.data;

        console.log($scope.subscriptions);
        $scope.hasProducts = $scope.subscriptions.length > 0 || $scope.accessories.length > 0;
    }
    function successAccessories(response) {
        var categories = [];

        for (var i = 0; i < response.data.length; ++i) {

            var added = false;
            for (var j = 0; j < categories.length; ++j) {

                if (categories[j].category === response.data[i].categorie) {
                    categories[j].accessories.push(response.data[i]);
                    added = true;
                }
            }

            if (!added) {
                categories.push({ category: response.data[i].categorie, accessories: [] });
                categories[j].accessories.push(response.data[i]);
            }
        }

        $scope.accessories = categories;

        console.log($scope.accessories);
        // S$scope.hasProducts = $scope.subscriptions.length > 0 || $scope.accessories.length > 0;
    }

    //Otherwise we call this one
    function error(response) {
        //If it fails, an error message will be printed to console
        console.log("Failed reading json (error: " + response.status + " => " + response.statusText + ")");
    }

}]);
