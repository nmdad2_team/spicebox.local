![Logo](http://users.telenet.be/timvanmeirvenne/spicebox/logo.jpg)

# Info
**Personen:** Aline Vercruysse & Tim Van Meirvenne

**Opleidingsonderdeel:** New Media Design & Development II

**Academiejaar:** 2015-2016

**Opleiding:** Bachelor in de grafische en digitale media

**Afstudeerrichting:** Multimediaproductie

**Keuzeoptie:** proDEV

**Opleidingsinstelling:** Arteveldehogeschool


#Repository
https://bitbucket.org/nmdad2_team/spicebox.local

#Briefing

Ontwerp en ontwikkel in groep van maximaal 2 studenten een innovatieve, thematische webwinkel gemaakt met de technologieën die tijdens de hoor- en werkcolleges aan bod komen.

#Planning
#Boards
###Ideaboard & Moodboard
https://www.pinterest.com/alineskywalker/nmdad-2/
###Style Tiles
####Style Tile A (gekozen Style Tile)
![enter image description here](http://users.telenet.be/timvanmeirvenne/spicebox/style1.jpg)
####Style Tile B
![enter image description here](http://users.telenet.be/timvanmeirvenne/spicebox/style2.jpg)
####Style Tile C
![enter image description here](http://users.telenet.be/timvanmeirvenne/spicebox/style3.jpg)
#Werkstuk
#Databasemodel

#API

###Functionaliteiten
| Route | Respons |
|-------|------:|
| api/products | Alle producten |
| api/product/{id}|   Bepaald product adhv id|
| api/abonnementen |    Alle abonnementen |



#App
###Functionaliteiten
###Wireflow
![enter image description here](http://users.telenet.be/timvanmeirvenne/spicebox/wireflow.png)
###Screen Designs

#Backoffice

###Functionaliteiten
####Dashboard
 - Tonen van grootste klanten (tabel)
 - Tonen van best verkochte producten (tabel)
 - Omzet van de laatste maanden tonen (grafiek)
 - Links naar de verschillende onderdelen van de pagina
####Productbeheer
 - Alle producten bekijken (tabel) + sorteren op een veld
 - Nieuw product toevoegen
 - Product verwijderen
 - Product aanpassen
 - Details van product zien (inclusief prijshistoriek)
####Klantenbeheer
 - Alle klanten bekijken (tabel) + sorteren op een veld
 - Klant verwijderen
####Orderbeheer
 - Alle orders bekijken, gescheiden op order die klaar zijn of niet klaar
 - Detail van een order bekijken
 - Order aanvinken zodat het de status klaar krijgt
 
###Sitemap

![enter image description here](http://users.telenet.be/timvanmeirvenne/spicebox/sitemap.png "Sitemap")
###Screen Designs
![enter image description here](http://users.telenet.be/timvanmeirvenne/spicebox/dashboard.PNG)
![enter image description here](http://users.telenet.be/timvanmeirvenne/spicebox/Products.PNG)
![enter image description here](http://users.telenet.be/timvanmeirvenne/spicebox/detail%20product.PNG)
![enter image description here](http://users.telenet.be/timvanmeirvenne/spicebox/orders.PNG)
![enter image description here](http://users.telenet.be/timvanmeirvenne/spicebox/detail%20order.PNG)


