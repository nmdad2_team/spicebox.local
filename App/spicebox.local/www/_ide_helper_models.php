<?php
/**
 * An helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Category
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 */
	class Promotion extends \Eloquent {}
	class Btw extends \Eloquent {}
	class Category extends \Eloquent {}
	class Price extends \Eloquent {}
	class Woonplaatsen extends \Eloquent {}
	class Postcodes extends \Eloquent {} 
	class Klanten extends \Eloquent {}
	class Orders extends \Eloquent {}
	class OrderStatus extends \Eloquent {}
	class Abonnees extends \Eloquent {}
	class Abonnees_opties extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Post
 *
 * @property-read \App\Models\Category $category
 * @property-read \App\Models\Promotion $promotions
 * @property-read \App\Models\Btw $BTW
 * @property-read \App\Models\Price $price
 * @property-read \App\Models\Woonplaats $woonplaats
 * @property-read \App\Models\Postcode $postcodes
 * @property-read \App\Models\Adress $adress
 * @property-read \App\Models\Klanten $klanten
 * @property-read \App\Models\Order $order
 * @property-read \App\Models\OrderStatus $orderstatus
 * @property-read \App\Models\Abonnees $abonnees
 * @property-read \App\Models\Abonnees_opties $abonnees_opties
 * @property-read \App\User $user
 */
	class Post extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Tag
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 */
	class Tag extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Post[] $posts
 */
	class User extends \Eloquent {}
}

