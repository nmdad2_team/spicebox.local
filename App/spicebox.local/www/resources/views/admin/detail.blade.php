@extends('admin')
<?php
$page_path = ['Products'] ;
$page_url = 'Detail';
$rating = $product[0]->rating
?>
@section('content')
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">Detail product</div>
        </div>
        <div class="panel-body detail">
<div class="col-md-10">

</div>
            <div class="col-md-2 text-center">
                <a href="/admin/product/detail/{{$product[0]->id}}/update"> <button class="btn btn-success "><i class="glyphicon glyphicon-pencil"></i></button></a>
                <a href="/admin/product/prijshistoriek/{{$product[0]->id}}"> <button class="btn btn-info "><i class="glyphicon glyphicon-euro"></i></button></a>
            </div>

                        <div class="col-md-12 text-center">
                          <img src="{{$product[0]->imgURL}}"/>
                        </div>


                        <label class="col-md-1 control-label text-center" for="name">Name</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text" placeholder="Product name" class="form-control input-md" required="" value="{{$product[0]->name}}">
                        </div>

                        <label class="col-md-1 control-label  text-center" for="name">Category</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text"  class="form-control input-md" required="" value="{{$product[0]->categorie}}">
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Price</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text" class="form-control input-md" required="" value="€{{$product[0]->price}}">
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Btw</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text"  class="form-control input-md" required="" value="{{$product[0]->btw}}%">
                        </div>

                            <label class="col-md-1 control-label text-center" for="name">Promotion</label>
                            <div class="col-md-11">
                                <input disabled id="name" name="name" type="text"  class="form-control input-md" required="" value="{{$product[0]->promotion_description}}">
                            </div>


                        <label class="col-md-1 control-label text-center" for="name">Description</label>
                        <div class="col-md-11">
                            <textarea disabled>{{$product[0]->description}}</textarea >
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Specifications</label>
                        <div class="col-md-11">
                            <textarea disabled>{{$product[0]->specifications}}</textarea >
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Rating</label>
                        <div class="col-md-11">
                           <p>
                               @for ($i=0;$i< $rating;$i++)
                                   <i class="glyphicon glyphicon-star"></i>
                               @endfor
                               @for ($i=0;$i < (5 - $rating);$i++)
                                   <i class="glyphicon glyphicon-star-empty"></i>
                               @endfor
                           </p>

                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Created at</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text"  class="form-control input-md" required="" value="{{$product[0]->created_at}}">
                        </div>

                        <label class="col-md-1 control-label text-center" for="name">Updated at</label>
                        <div class="col-md-11">
                            <input disabled id="name" name="name" type="text"  class="form-control input-md" required="" value="{{$product[0]->updated_at}}">
                        </div>


        </div>
    </div>
@endsection