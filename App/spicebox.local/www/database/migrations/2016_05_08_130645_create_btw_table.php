<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBtwTable extends Migration
{

    const TABLE = 'btws';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('btws', function (Blueprint $table) {
            $table->increments('id_btw');
            $table->integer('btw');
            $table->timestamps();
        });
        Schema::table(self::TABLE, function ($table) {
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('btws');
    }
}
