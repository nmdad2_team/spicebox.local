﻿

//Create module
var mainApp = angular.module('mainMod', ['userMod', 'ngMaterial'])
//-----SETUP THEME----//
.config(function ($mdThemingProvider, $mdIconProvider) {
        $mdThemingProvider.definePalette('spiceboxThemeRed', {
            '50': '#e16c6c',
            '100': '#dc5757',
            '200': '#d84141',
            '300': '#d42c2c',
            '400': '#bf2727',
            '500': '#aa2323',
            '600': '#951f1f',
            '700': '#801a1a',
            '800': '#6b1616',
            '900': '#551212',
            'A100': '#e58181',
            'A200': '#e99696',
            'A400': '#000000',
            'A700': '#ffffff'
        });
        $mdThemingProvider.definePalette('spiceboxThemeYellow', {
            '50': '#fdd89c',
            '100': '#fcce83',
            '200': '#fcc46a',
            '300': '#fbba51',
            '400': '#fbb038',
            '500': '#faa61f',
            '600': '#f99c06',
            '700': '#e18d05',
            '800': '#c87d04',
            '900': '#af6d04',
            'A100': '#fde1b5',
            'A200': '#feebce',
            'A400': '#fef5e7',
            'A700': '#965e03'
        });
        $mdThemingProvider.definePalette('spiceboxThemeBackground', {
            '50': '#ffffff',
            '100': '#ffffff',
            '200': '#ffffff',
            '300': '#f8f5f3',
            '400': '#eee7e4',
            '500': '#e4d9d4',
            '600': '#dacbc4',
            '700': '#d0bdb5',
            '800': '#c6afa5',
            '900': '#bda295',
            'A100': '#ffffff',
            'A200': '#ffffff',
            'A400': '#ffffff',
            'A700': '#000000'
        });
        $mdThemingProvider.theme('default')
          .warnPalette('spiceboxThemeYellow'), {
              'default': '500',
              'hue-1' : '400',
              'hue-2' : '300',
              'hue-3' : '50'
          }
        $mdThemingProvider.theme('default')
           .primaryPalette('spiceboxThemeRed'), {
               'default': '500',
               'hue-1': '400',
               'hue-2': 'A400',
               'hue-3': 'A700'
             }
        $mdThemingProvider.theme('default')
           .backgroundPalette('spiceboxThemeBackground'), {
               'default': '500',
               'hue-1': 'A100',
               'hue-2': 'A700',
               'hue-3': '50'
             }
});

//DIRECTIVES
//===========
mainApp.directive('products', function() {

    return {
        restrict: 'E',
        templateUrl: 'htmlPages/products.html'
    };
});
mainApp.directive('cart', function() {

    return {
        restrict: 'E',
        templateUrl: 'htmlPages/cart.html'
    };
});
mainApp.directive('home', function () {

    return {
        restrict: 'E',
        templateUrl: 'htmlPages/home.html'
    };
});
mainApp.directive('howitworks', function () {

    return {
        restrict: 'E',
        templateUrl: 'htmlPages/howitworks.html'
    };
});
mainApp.directive('join', function () {

    return {
        restrict: 'E',
        templateUrl: 'htmlPages/join.html'
    };
});
mainApp.directive('profile', function () {

    return {
        restrict: 'E',
        templateUrl: 'htmlPages/profile.html'
    };
});
mainApp.directive('navDesktop', function () {

    return {
        restrict: 'E',
        templateUrl: 'htmlTemplates/nav-desktop.html'
    };
});
mainApp.directive('navMobile', function () {

    return {
        restrict: 'E',
        templateUrl: 'htmlTemplates/nav-mobile.html'
    };
});
mainApp.directive('subtitle', function () {

    return {
        restrict: 'E',
        templateUrl: 'htmlTemplates/subtitle.html'
    };
});
mainApp.directive('accessorypage', function () {

    return {
        restrict: 'E',
        templateUrl: 'htmlPages/accessorypage.html'
    };
});
mainApp.directive('subscriptionpage', function () {

    return {
        restrict: 'E',
        templateUrl: 'htmlPages/subscriptionpage.html'
    };
});

//Create a controller to manage the main page content
mainApp.controller('MainController', function () {

    //Variables
    this.currentPage = 'Home';
    //title decides what to show as subtitle
    this.title = 'Spice up your life \r\n with our awesome Spiceboxes!';

    //Remember last navigated page and title (used when the user decides to login/register)
    this.previousPage = 'Home';
    this.previousTitle = 'Spice up your life \r\n with our awesome Spiceboxes!';

    this.HOMEID = 'Home';
    this.PRODUCTSID = 'Products';
    this.HOWID = 'How';
    this.JOINID = 'Join';
    this.PROFILEID = 'Profile';
    this.CARTID = 'Cart';
    this.SUBSCRIPTIONID = 'Subscription';
    this.ACCESSORYID = 'Accessory';

    this.subscriptionDetail = undefined;
    this.accessoryDetail = undefined;

    //Functions
    //==========
    //Functions below are used to set specific content visible (active)
    //Each function will also set the correct subtitle for that content


    this.setHomePageActive = function () {
        this.setPageActive(this.HOMEID, 'Spice up your life \r\n with our awesome Spiceboxes!');
    }
    this.setProductsPageActive = function () {
        this.setPageActive(this.PRODUCTSID, 'Our Products');
    }
    this.setHowPageActive = function () {
        this.setPageActive(this.HOWID, 'How it works');
    }
    this.setJoinPageActive = function () {
        this.setPageActive(this.JOINID, 'Join');
    }
    this.setProfilePageActive = function () {
        this.setPageActive(this.PROFILEID, 'Profile');
    }
    this.setCartPageActive = function () {
        this.setPageActive(this.CARTID, 'Cart');
    }
    this.setSubscriptionPageActive = function (subscription) {
        if (subscription != undefined) {
            this.subscriptionDetail = subscription;
            this.setPageActive(this.SUBSCRIPTIONID, subscription.name);
        }
    }
    this.setAccessoryPageActive = function (accessory) {
        if (accessory != undefined) {
            this.accessoryDetail = accessory;
            this.setPageActive(this.ACCESSORYID, accessory.name);
        }
    }

    this.setLastPageActive = function() {
        this.setPageActive(this.previousPage, this.previousTitle);
    }

    //This is the function to change the subtitle and current page
    this.setPageActive = function(page, title) {
        this.previousPage = this.currentPage;
        this.currentPage = page;
        this.previousTitle = this.title;
        this.title = title;

        if (this.currentPage !== this.SUBSCRIPTIONID)
            this.subscriptionDetail = undefined;
        if (this.currentPage !== this.ACCESSORYID)
            this.accessoryDetail = undefined;
    }

    this.isCurrentlyHomePage = function () {
        return this.currentPage === this.HOMEID;
    }
    this.isCurrentlyProductsPage = function () {
        return this.currentPage === this.PRODUCTSID;
    }
    this.isCurrentlyHowPage = function () {
        return this.currentPage === this.HOWID;
    }
    this.isCurrentlyJoinPage = function () {
        return this.currentPage === this.JOINID;
    }
    this.isCurrentlyProfilePage = function () {
        return this.currentPage === this.PROFILEID;
    }
    this.isCurrentlyCartPage = function () {
        return this.currentPage === this.CARTID;
    }
    this.isSubscriptionPageActive = function () {
        return this.currentPage === this.SUBSCRIPTIONID;
    }
    this.isAccessoryPageActive = function () {
        return this.currentPage === this.ACCESSORYID;
    }
});
mainApp.controller('MobileNavController', function ($mdDialog) {
   // console.log(foo); // bar
    var originatorEv;
    this.openMenu = function ($mdOpenMenu, ev) {
        originatorEv = ev;
        $mdOpenMenu(ev);
    };
});
