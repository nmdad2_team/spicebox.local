<?php
use App\Models\Adress;
use Illuminate\Database\Seeder;

class AdressTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Adress::create([
            'straatnaam' => "Kerkstraat",
            'huisnummer' => rand(0,255),
            'id_postcode' => rand(1,3),
            'id_klant' => 1,
        ]);
        Adress::create([
            'straatnaam' => "Vogeltjeslaan",
            'huisnummer' => rand(0,255),
            'id_postcode' => rand(1,3),
            'id_klant' => 2,
        ]);
        Adress::create([
            'straatnaam' => "Stationsstraat",
            'huisnummer' => rand(0,255),
            'id_postcode' => rand(1,3),
            'id_klant' => 3,
        ]);
        Adress::create([
            'straatnaam' => "Rozenlaan",
            'huisnummer' => rand(0,255),
            'id_postcode' => rand(1,3),
            'id_klant' => 4,
        ]);
        Adress::create([
            'straatnaam' => "Grote Markt",
            'huisnummer' => rand(0,255),
            'id_postcode' => rand(1,3),
            'id_klant' => 5,
        ]);
        Adress::create([
            'straatnaam' => "Waterschootstraat",
            'huisnummer' => rand(0,255),
            'id_postcode' => rand(1,3),
            'id_klant' => 6,
        ]);
        Adress::create([
            'straatnaam' => "Molenlaan",
            'huisnummer' => rand(0,255),
            'id_postcode' => rand(1,3),
            'id_klant' => 7,
        ]);
        Adress::create([
            'straatnaam' => "Apostelplein",
            'huisnummer' => rand(0,255),
            'id_postcode' => rand(1,3),
            'id_klant' => 8,
        ]);
        Adress::create([
            'straatnaam' => "Bierstraat",
            'huisnummer' => rand(0,255),
            'id_postcode' => rand(1,3),
            'id_klant' => 9,
        ]);
        Adress::create([
            'straatnaam' => "Kalkenstraat",
            'huisnummer' => rand(0,255),
            'id_postcode' => rand(1,3),
            'id_klant' => 10,
        ]);




    }
}
