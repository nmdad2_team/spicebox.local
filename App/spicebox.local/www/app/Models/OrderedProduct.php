<?php

namespace App\Models;

use App\User;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderedProduct extends Model
{
    use SoftDeletes;
    protected $table = 'orderedproducts';
    // Relationships
    // =============
    /**
     * Many-to-One.
     *
     * @link https://laravel.com/docs/5.2/eloquent-relationships#one-to-many
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function products()
    {
        return $this->hasOne(Product::class);
    }
    public function orders()
    {
        return $this->hasOne(Order::class);
    }

}