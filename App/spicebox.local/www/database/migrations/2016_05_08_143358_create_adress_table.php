<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adress', function (Blueprint $table) {
            $table->increments('id');
            $table->text('straatnaam');
            $table->integer('huisnummer');
            $table->integer('id_postcode')->unsigned();
            $table->integer('id_klant')->unsigned();


            // Foreign Keys
            $table->foreign('id_postcode')->references('id')->on('postcodes');
            $table->foreign('id_klant')->references('id')->on('klanten');


            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('adress');
    }
}
