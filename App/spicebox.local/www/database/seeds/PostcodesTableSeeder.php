<?php
use App\Models\Postcode;
use Illuminate\Database\Seeder;

class PostcodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Postcode::create([
        'postcode' => 1000,
        'id_woonplaats' => 1,
    ]);
        Postcode::create([
            'postcode' => 2000,
            'id_woonplaats' => 2,
        ]);
        Postcode::create([
            'postcode' => 3000,
            'id_woonplaats' => 3,
        ]);
    }
}
