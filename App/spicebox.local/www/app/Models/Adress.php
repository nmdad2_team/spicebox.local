<?php

namespace App\Models;

use App\User;
use App\Models\Postcode;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Adress extends Model
{
    use SoftDeletes;
    protected $table = 'adress';
    protected $fillable = array('straatnaam', 'huisnummer', 'id_postcode', 'id_klant');

    // Relationships
    // =============
    /**
     * Many-to-One.
     *
     * @link https://laravel.com/docs/5.2/eloquent-relationships#one-to-many
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function postcodes()
    {
        return $this->belongsTo(Postcode::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}