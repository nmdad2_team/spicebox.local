﻿//Create a module to contain all data in the cart
var cartModule = angular.module('cartMod', ['productsMod']);

cartModule.directive('cartsubscriptions', function() {

    return {
        restrict: 'E',
        scope: {
            subscriptions: '=',
            cartCtrl: '='
        },
        templateUrl: 'htmlTemplates/subscriptions/subscriptions-cart.html',
        link: function (scope, elem, attrs) {

            scope.removeFromCart = function (index) {
                scope.cartCtrl.removeSubscription(index);
            };
        }
    };
});
cartModule.directive('cartaccessories', function () {

    return {
        restrict: 'E',
        scope: {
            accessories: '=',
            cartCtrl: '='
        },
        templateUrl: 'htmlTemplates/accessories/accessories-cart.html',
        link: function (scope, elem, attrs) {

            scope.removeFromCart = function (index) {
                scope.cartCtrl.removeAccessorySingle(scope.accessories[index].accessory);
            };
            scope.removeAllFromCart = function (index) {
                scope.cartCtrl.removeAccessoryAll(scope.accessories[index].accessory);
            };
        }
    };
});

cartModule.controller('CartController', ['$http', function($http) {

    //ACCESSORIES
    //============
    //List of accessories in the cart
    this.accessories = [];
    //Helper function to prevent miscalculations in HTML
    this.hasAccessories = function () {
        return this.accessories.length > 0;
    }

    //Add/remove accessories
    this.addAccessory = function (accessory) {

        //First check if an accessory is already in the cart (to avoid a lot of same item visible)
        var cartItem = this.getAccessoryFromCart(accessory);
        if (cartItem != undefined)
            //If there is, just increase the amount in the cart of that item
            ++cartItem.amount;
        else
            //If not yet in cart, add it to the cart (and setting amount to 1)
            this.accessories.push({ accessory: accessory, amount: 1 });
    }
    this.removeAccessorySingle = function (accessory) {

        //Check if the accessory is in the cart, can't remove what is not there
        var cartItem = this.getAccessoryFromCart(accessory);
        if (cartItem != undefined) {
            //Decrease the amount in the cart
            --cartItem.amount;

            //If the amount left in the cart is 0 (or somehow negative)
            if (cartItem.amount <= 0)
                //Remove it completely from the cart
                this.removeAccessoryAll(accessory);
        }
    }
    this.removeAccessoryAll = function (accessory) {

        //Loop over all accessories in the cart
        for (i = 0; i < this.accessories.length; ++i) {
            //Match the cart accessory with the name of the given accessory
            if (this.accessories[i].accessory === accessory) {
                //Geat it out of the list
                this.accessories.splice(i, 1);
                break;
            }
        }
    }

    //Get cart accessory
    this.getAccessoryFromCart = function (accessory) {

        //Loop over all accessories in the cart
        for (i = 0; i < this.accessories.length; ++i) {
            //Match the cart accessory with the name of the given accessory
            if (this.accessories[i].accessory === accessory) {
                //Found it
                return this.accessories[i];
            }
        }

        //Couldn't find it
        return undefined;
    }

    //SUBSCRIPTIONS
    //==============
    //List of subscriptions in cart
    this.subscriptions = [];
    //Helper function to prevent miscalculations in HTML
    this.hasSubscriptions = function () {
        return this.subscriptions.length > 0;
    }

    //Add/remove subscription
    this.addSubscription = function (subscription) {

        //Add each subscription seperately, so the user can set the autorenew on each seperately
        this.subscriptions.push(subscription);
    }
    this.removeSubscription = function (index) {

        if (index >= 0 && index < this.subscriptions.length) {
            this.subscriptions.splice(index, 1);
        }
    }

    //Get cart subscriptions
    this.getSubscriptionFromCart = function (subscription) {

        //Loop over all subscriptions in the cart
        for (i = 0; i < this.subscriptions.length; ++i) {
            //Match the cart subscription the given accessory
            if (this.subscriptions[i] === subscription) {
                //Found it
                return this.subscriptions[i];
            }
        }

        //Couldn't find it
        return undefined;
    }

    //GENERAL
    //========
    //Calculates total cart price
    this.btwExclPrice = function() {
        //Nothing in cart by default
        var price = 0;
        //Loop over all accessories (if any)
        for (i = 0; i < this.accessories.length; ++i) {

            //Copy it out of the cart (easier to work with)
            var cartContent = this.accessories[i];
            //Increase the total price (parse to float, otherwise they are considered strings)
            var reducedPrice = parseFloat(cartContent.accessory.price) * (parseFloat(cartContent.accessory.promotion) / 100);
            price += (reducedPrice * parseFloat(cartContent.amount));
        }

        //Loop over all subscriptions (if any)
        for (i = 0; i < this.subscriptions.length; ++i) {

            //Copy it out of the cart
            var cartContent = this.subscriptions[i];
            //Increase the total price (parse to float, otherwise they are considered strings)
            price += parseFloat(cartContent.price);
        }
        //Give back the calculated price
        return price;
    }
    this.totalPrice = function () {

        //Nothing in cart by default
        var price = 0;
        //Loop over all accessories (if any)
        for (i = 0; i < this.accessories.length; ++i) {

            //Copy it out of the cart (easier to work with)
            var cartContent = this.accessories[i];
            //Increase the total price (parse to float, otherwise they are considered strings)
            var reducedPrice = parseFloat(cartContent.accessory.price) * (parseFloat(cartContent.accessory.promotion) / 100);
            var withBtw = reducedPrice + (reducedPrice * (parseFloat(cartContent.accessory.btw) / 100));
            price += (withBtw * parseFloat(cartContent.amount));
        }

        //Loop over all subscriptions (if any)
        for (i = 0; i < this.subscriptions.length; ++i) {

            //Copy it out of the cart
            var cartContent = this.subscriptions[i];
            //Increase the total price (parse to float, otherwise they are considered strings)
            price += parseFloat(cartContent.price);
        }
        //Give back the calculated price
        return price;
    }

    //Checking out means adding all needed data to db and user profile if needed
    this.checkout = function(userCtrl) {

        var totalprice = this.totalPrice(this);
        var orderdate =  "2016-9-08";
        var orderstatus = 1;

        // To Do ID klant
        var id_klant = 1;


        var data = { totalprice : totalprice , orderdate : orderdate, orderstatus:orderstatus, id_klant: id_klant};
        console.log(data);
        $http({
            method: 'POST',
            data: data,
            url: './order'
        }).then(function(response){
            console.log(response);
        }, function(response) {
            console.log('Failed: ' + response.status + " " + response.message);
        });


        //Don't order for nobody that doesn't wish to log in
        if (userCtrl === undefined || userCtrl === null)
            return;

        //Add all bought accessories to purchase history
        for (var i = 0; i < this.accessories.length; ++i) {
            var accessory = this.accessories[i];
            userCtrl.addNewPurchase(accessory.accessory, accessory.amount);
        }
        //Clear cart of accessories
        this.accessories = [];

        //Add all bought subscriptions to subscription list
        for (var i = 0; i < this.subscriptions.length; ++i) {

            var sub = this.subscriptions[i];
            userCtrl.addNewSubscription(sub);

        }
        //Clear cart of accessories
        this.subscriptions = [];
    }
}]);
