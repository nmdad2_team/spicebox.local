<?php

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'categorie' => 'Kruiden',
        ]);
        Category::create([
            'categorie' => 'Kruidenpotten',
        ]);
        Category::create([
            'categorie' => 'Divers kookgerei',
        ]);
        Category::create([
            'categorie' => 'Andere',

        ]);
    }
}
