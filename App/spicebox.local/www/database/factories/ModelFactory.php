<?php

use App\User;
use App\Models\Category;
use App\Models\Price;
use App\Models\Btw;
use App\Models\Product;
use App\Models\Woonplaats;
use App\Models\Postcode;
use App\Models\Adress;
use App\Models\Klant;
use App\Models\Order;
use App\Models\OrderedProduct;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Category::class, function (Faker $faker) : array {
    return [
        'name' => $faker->unique()->word(),
        'description' => $faker->paragraph($nbSentences = 3),
    ];
});

$factory->define(User::class, function (Faker $faker) : array {
    return [
        'email' => $faker->safeEmail,
        'password' => Hash::make($faker->password($minLength = 6, $maxLength = 20)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Price::class, function (Faker $faker) : array {
    return [
        'price' => rand(10,100)
    ];
});

$factory->define(Product::class, function (Faker $faker) : array {
    $price = rand(1,100);
    return [
        'name' => $faker->name,
        'imgURL' => $faker->imageUrl(),
        'description' => $faker->text(10),
        'specifications' => $faker->text(10),
        'price' => $price,
        'aantal_verkocht' => rand(0,100),
        'id_promotion' => rand(1,4),
        'id_categorie' => rand(1,4),
        'id_btw' => rand(1,4),
        'rating' => rand(0,5),
        'prijshistoriek' => '{"Originele prijs":"'. $price . '"}',
    ];
});

$factory->define(Woonplaats::class, function (Faker $faker) : array {
    return [
        'woonplaats' => $faker->city
    ];
});

$factory->define(Postcode::class, function (Faker $faker) : array {

    return [
        'postcode' => rand(1000,9999),
        'id_woonplaats' => $faker->numberBetween(1,10)
    ];
});

$factory->define(Klant::class, function (Faker $faker) : array {
    return [
        'naam' => $faker->name,
        'aantal_bestellingen' => $faker->numberBetween(0,50),
        'id_user' => $faker->unique()->numberBetween(1,10),
    ];
});

$factory->define(Order::class, function (Faker $faker) : array {
    $month = $faker->numberBetween(3,5);
    return [
        'totalprice' => $faker->numberBetween(50,150),
        'id_orderstatus' => $faker->numberBetween(1,2),
        'id_klant' => $faker->numberBetween(1,10),
        'orderdate' => $faker->date('2016-'. $month .'-d')
    ];
});
    $factory->define(OrderedProduct::class, function (Faker $faker) : array {
        $id = $faker->numberBetween(1, 10);
        return [
            'id_product' => $faker->numberBetween(1, 10),
            'aantal' => $faker->numberBetween(1, 3),
            'id_order' => $id
        ];
    });
