@extends('admin')
        <?php $page_title = "Klantenbeheer";

        $page_path = [] ;
        $page_url = 'Klantenbeheer';
        ?>
        @section('content')

            <div class="container">

                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-striped">
                            <thead>

                                <td><a href="/admin/klanten/sort/naam">Naam</a></td>
                                <td><a href="/admin/klanten/sort/straatnaam">Adress</a></td>
                                <td><a href="/admin/klanten/sort/postcode">Postcode</a></td>
                                <td><a href="/admin/klanten/sort/woonplaats">Woonplaats</a></td>
                            </thead>
                            <tbody>

                            @foreach ($klanten as $k)
                                <tr>
                                    <td>{{$k->naam}}</td>
                                    <td>{{$k->straatnaam}} {{$k->huisnummer}}</td>
                                    <td>{{$k->postcode}}</td>
                                    <td>{{$k->woonplaats}}</td>
                                    <td><a href="/admin/klant/destroy/{{$k->id}}"><button class="btn btn-danger"><i class = "glyphicon glyphicon-remove"></i></button></a> </td>
                                </tr>

                            @endforeach
                            </tbody>

                        </table>
                    </div>
                </div>
@endsection