<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'admin@spicebox.local',
            'password' => bcrypt('appelmoes'),
        ]);

        // Faker
        // -----
        factory(User::class, DatabaseSeeder::AMOUNT['DEFAULT'])->create();
    }
}
