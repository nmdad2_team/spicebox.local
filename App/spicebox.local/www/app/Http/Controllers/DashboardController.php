<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Klant;
use App\Models\Product;
use App\Http\Requests;

class DashboardController extends Controller
{
    public function index(){
        $orders = Order::where('id_orderstatus',2)
            ->get();
        $klanten = Klant::get();
        $products = Product::get();
        $topP = Product::orderBy('aantal_verkocht', 'des')
            ->get();
        $topK = Klant::orderBy('aantal_bestellingen', 'des')
            ->get();
        $orderGeschiedenis = Order::orderBy('orderdate')
        ->get();
        $arrMaand = array();
        foreach ($orderGeschiedenis as $o) {
           $month = substr($o->orderdate,5,1);
            if ( ! isset($arrMaand[$month])) {
                $arrMaand[$month] = $o -> totalprice;
            }
            else{

                $arrMaand[$month] += $o -> totalprice;
            }

        }

        return view('admin.dashboard', ['orders' => $orders, 'klanten' => $klanten, 'products' => $products, 'topP' => $topP, 'topK' => $topK, 'arrMaand' => $arrMaand]);
    }

}
