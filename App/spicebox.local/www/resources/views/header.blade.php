<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="/" class="logo">Spicebox</a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account Menu -->
                <li class="user user-menu">
                    <!-- Menu Toggle Button -->

                    <a href="/logout">
                        Log out
                    </a>


                </li>
            </ul>
        </div>
    </nav>
</header>