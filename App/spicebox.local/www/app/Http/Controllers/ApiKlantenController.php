<?php

namespace App\Http\Controllers;

use App\Models\Adress;
use App\Models\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Klant;
use App\Models\Postcode;
use App\Models\Woonplaats;

use App\User;

use League\Flysystem\Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use HttpResponse;
use JWTAuth;
use Hash;
use Auth;



class ApiKlantenController extends Controller
{
    public function index(){
        return Klant::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function registerKlant() {
        $credentials = Input::all();
        $credentials['password'] = Hash::make($credentials['password']);
        try {
            $user =  User::create([
                'email' => $credentials["email"],
                'password' => $credentials["password"],
            ]);

        } catch (Exception $e) {
            return Response::json(['error' => 'User already exists.'], HttpResponse::HTTP_CONFLICT);
        }
        $token = JWTAuth::fromUser($user);

        $id_user_temp = User::where('email', $credentials['email'])->pluck('id');
        $id_user = intval($id_user_temp[0]);


        Klant::create([
            'naam' => $credentials["name"],
            'aantal_bestellingen' => 0,
            'id_user' => $id_user,
        ]);
        $id_klant_temp = Klant::where('naam', $credentials['name'])->pluck('id');
        $id_klant = intval($id_klant_temp[0]);


        if (Postcode::where('postcode',$credentials['postcode'])->exists()) {
            $id_postcode_temp = Postcode::where('postcode',$credentials['postcode'])->get();
            $id_postcode = $id_postcode_temp[0]['id'];

            Adress::create([
                'straatnaam' => $credentials["street"],
                'huisnummer' => $credentials["number"],
                'id_postcode' => $id_postcode,
                'id_klant' => $id_klant,
            ]);
        }
        else
        {
            $rows = Postcode::count() + 1;
            Woonplaats::create([
                'woonplaats' => $credentials['city'],
            ]);
            Postcode::create([
                'postcode' => $credentials['postcode'],
                'id_woonplaats' => $rows,
            ]);
            Adress::create([
                'straatnaam' => $credentials["street"],
                'huisnummer' => $credentials["number"],
                'id_postcode' => $rows,
                'id_klant' => $id_klant,
            ]);

        }
        return Response::json(compact('token'));

    }
    public function signin() {
        $credentials = Input::only('email', 'password');
        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        // if no errors are encountered we can return a JWT

        return response()->json(compact('token'));
    }
    public function getKlant(Request $request){
            $id = Auth::user()->id;
        if (Auth::guest())
            return 'guest';
        $klant = Klant::
        join('adress', 'klanten.id', '=', 'adress.id_klant')
            ->leftJoin('postcodes', 'adress.id_postcode', '=', 'postcodes.id')
            ->leftJoin('woonplaatsen', 'postcodes.id_woonplaats', '=', 'woonplaatsen.id_woonplaats')
            ->where('id_user', ($id))
            ->get();
        return json_encode($klant);
    }
    public function updateKlant() {
        $id = Auth::user()->id;
        $data = Input::all();
        $id_klant_temp = Klant::where('id_user', $id)->pluck('id');
        $id_klant = intval($id_klant_temp[0]);
        // $id_postcode_temp = Adress::where('id_klant', $id_klant)->pluck('id_postcode');
        //

        if (Postcode::where('postcode',$data['postcode'])->exists()) {

            $id_postcode_temp = Postcode::where('postcode',$data['postcode'])->get();
            $id_postcode = $id_postcode_temp[0]['id'];
            Adress::where('id_klant',$id_klant)
                ->update(array(
                    'id_postcode'=> $id_postcode,
                ));

        }
        else
        {
            $rows = Postcode::count() + 1;
            Woonplaats::create([
                'woonplaats' => $data['woonplaats'],
            ]);
            Postcode::create([
                'postcode' => $data['postcode'],
                'id_woonplaats' => $rows,
            ]);
            Adress::where('id_klant',$id_klant)
                ->update(array(
                    'id_postcode'=> $rows,
                ));
        }

        Klant::where('id_user', $id)
            ->update(array(
                'naam' => $data["naam"],
            ));

        Adress::where('id_klant',$id_klant)
            ->update(array(
                'straatnaam'=> $data["straatnaam"],
                'huisnummer'=> $data["huisnummer"],
            ));
return "ok";


    }
}
