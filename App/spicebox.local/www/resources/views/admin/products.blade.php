@extends('admin')
<?php $page_title = "Productbeheer";

$page_path = [] ;
$page_url = 'Products';
?>
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-11"></div>
            <div class="col-md-1">
                <a href="/admin/product/new"><button class="btn btn-success"><i class="glyphicon glyphicon-plus"></i></button></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <td><a href="/admin/products/sort/id">id</a></td>
                    <td><a href="/admin/products/sort/name">Naam</a></td>
                    <td><a href="/admin/products/sort/price">Prijs</a></td>
                    <td><a href="/admin/products/sort/btw">Btw</a></td>
                    <td><a href="/admin/products/sort/promotion_description">Promotie</a></td>
                    <td><a href="/admin/products/sort/categorie">Categorie</a></td>
                    <td><a href="/admin/products/sort/aantal_verkocht">x verkocht</a></td>
                    <td></td>
                    </thead>
                     <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td>{{$product->name}}</td>
                            <td>€{{$product->price}}</td>
                            <td>{{$product->btw}}%</td>
                            <td>{{$product->promotion_description}}</td>
                            <td>{{$product->categorie}}</td>
                            <td>{{$product->aantal_verkocht}}</td>
                            <td>
                                <a href="/admin/product/destroy/{{$product->id}}"><button class="btn btn-danger"><i class = "glyphicon glyphicon-remove"></i></button></a>
                                <a href="/admin/product/detail/{{$product->id}}/update"> <button class="btn btn-success "><i class="glyphicon glyphicon-pencil"></i></button></a>
                                <a href="/admin/product/detail/{{$product->id}}"><button class="btn btn-info"><i class="glyphicon glyphicon-eye-open"></i> </button></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection