<?php

namespace App\Models;

use App\User;
use App\Models\Woonplaats;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Postcode extends Model
{
    use SoftDeletes;
    protected $fillable = array('id_woonplaats', 'postcode');
    

    // Relationships
    // =============
    /**
     * Many-to-One.
     *
     * @link https://laravel.com/docs/5.2/eloquent-relationships#one-to-many
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function woonplaatsen()
    {
        return $this->belongsTo(Woonplaats::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}