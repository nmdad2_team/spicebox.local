<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderedProduct;
use Illuminate\Http\Request;

use App\Http\Requests;

class OrdersController extends Controller
{
    public function index(){
        $orders = Order::
            join('klanten', 'klanten.id', '=', 'orders.id_klant')
            ->join('orderstatus', 'orderstatus.id_orderstatus', '=', 'orders.id_orderstatus')
            ->select('orders.*','orders.totalprice','klanten.naam','orderstatus.status')
            ->get();
        return view('admin.orders',['orders' => $orders]);
    }

    public function finish($id){
        Order::where('id', $id)
            ->update(array(
                'id_orderstatus' => 1,
            ));

        return redirect('/admin/orders');
    }

    public function detail($id){
        $bestelling = OrderedProduct::where('id_order', $id)
            ->join('products', 'products.id', '=', 'orderedproducts.id_product')
            ->leftjoin('btws', 'btws.id_btw', '=', 'products.id_btw')
            ->leftjoin('promotions', 'promotions.id_promotion', '=', 'products.id_promotion')
            ->get();
        return view('admin.order',['bestelling' => $bestelling]);
    }
}
