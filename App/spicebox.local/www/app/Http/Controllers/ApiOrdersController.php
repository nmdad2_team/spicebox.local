<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\Order;
use App\Models\Klant;
use Auth;


class ApiOrdersController extends Controller
{
    public function index(){
        return Order::all();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $id_user = Auth::user()->id;
        $id_klant_temp = Klant::where('id_user', $id_user)->pluck('id');
        $id_klant = intval($id_klant_temp[0]);

        $order = new Order();
        $order->totalprice = $data['totalprice'];
        $order->orderdate = $data['orderdate'];
        $order->id_orderstatus = $data['orderstatus'];
        $order->id_klant =  $id_klant;


        $order->save();

        return Order::all()->count();

    }

}
