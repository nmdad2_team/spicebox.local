<?php
use App\Models\OrderedProduct;
use Illuminate\Database\Seeder;

class OrderedProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Faker
        // -----
        factory(OrderedProduct::class, DatabaseSeeder::AMOUNT['MANY'])->create();
    }
}
