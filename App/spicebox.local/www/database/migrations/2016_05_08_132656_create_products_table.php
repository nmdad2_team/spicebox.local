<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    const MODEL = 'product';
    const TABLE = self::MODEL.'s';
    const PRIMARY_KEY = 'id';
    const FOREIGN_KEY = self::MODEL.'_'.self::PRIMARY_KEY;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create(self::TABLE, function (Blueprint $table) {
            // Primary Key
            $table->increments(self::PRIMARY_KEY);

            // Data
            $table->string('name')->unique();

            $table->string('imgURL');
            $table->text('description');
            $table->text('specifications');
            $table->integer('price');
            $table->integer('rating');
            $table->integer('aantal_verkocht');
            $table->text('prijshistoriek');


            $table->integer('id_promotion')->unsigned();
            $table->integer('id_categorie')->unsigned();
            $table->integer('id_btw')->unsigned();



            // Foreign Keys
            $table->foreign('id_categorie')->references('id_categorie')->on('categories');
            $table->foreign('id_promotion')->references('id_promotion')->on('promotions');
            $table->foreign('id_btw')->references('id_btw')->on('btws');


            // Meta Data
            $table->timestamps(); // 'created_at', 'updated_at'
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(self::TABLE);
    }
}
