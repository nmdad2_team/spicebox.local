var toastModule = angular.module('toastMod', ['cartMod'])

toastModule.controller('AddToCartController', function($scope, $mdToast, $document) {

    this.showAddToCart = function(product) {
        $mdToast.show({
            template: '<md-toast><span flex>Added ' + product.name + ' to cart</span></md-toast>',
            parent: $document[0].querySelector('#addtocarttoast'),
            hideDelay: 3000,
            position: 'top right'
        });
    };
});
