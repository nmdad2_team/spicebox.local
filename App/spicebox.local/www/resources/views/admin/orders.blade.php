@extends('admin')
<?php $page_title = "Orderbeheer";

$page_path = [] ;
$page_url = 'Orderbeheer';
?>
@section('content')
    <div class="container">
        <div class="row">
            <h1>Niet klaar</h1>
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <td>id</td>
                    <td>Klant</td>
                    <td>Status</td>
                    <td>Aangemaakt op</td>
                    <td></td>
                    </thead>
                    <tbody>
                    @foreach ($orders as $o)
                        @if($o->status == "Niet klaar")
                        <tr>
                            <td>{{$o->id}}</td>
                            <td>{{$o->naam}}</td>
                            <td>{{$o->status}}</td>
                            <td>{{$o->orderdate}}</td>
                            <td>
                                <a href="/admin/order/{{$o->id}}/finish"><button class="btn btn-success "><i class="glyphicon glyphicon-ok"></i></button></a>
                                <a href="/admin/order/{{$o->id}}"><button class="btn btn-info"><i class="glyphicon glyphicon-eye-open"></i></button></a>
                            </td>
                        </tr>
                    @endif
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>

        <div class="row">
            <h1>Klaar</h1>
            <div class="col-md-12">
                <table class="table table-striped">
                    <thead>
                    <td>id</td>
                    <td>Klant</td>
                    <td>Status</a></td>
                    <td>Aangemaakt op</a></td>
                    <td></td>
                    </thead>
                    <tbody>
                    @foreach ($orders as $o)
                        @if($o->status == "Klaar")
                            <tr>
                                <td>{{$o->id}}</td>
                                <td>{{$o->naam}}</td>
                                <td>{{$o->status}}</td>
                                <td>{{$o->orderdate}}</td>
                                <td><a href="/admin/order/{{$o->id}}"><button class="btn btn-info"><i class="glyphicon glyphicon-eye-open"></i></button></a></td>

                            </tr>
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
@endsection